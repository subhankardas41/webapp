package com.subhankar.demo.webapp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Value("${greeting.message}")
    private String message;
    @Value("${status.message}")
    private String status;
    @GetMapping("/hello/{name}")
    public String hello(@PathVariable String name) {
        return message + " | " + name;
    }

    @GetMapping("/")
    public String test() {
        return status;
    }
}
